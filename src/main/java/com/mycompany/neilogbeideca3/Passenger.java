/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.neilogbeideca3;

/**
 *
 * @author Neil Ogbeide
 */



public class Passenger {
    private String name;
    private int ID;
    private String email;
    private String telephone;
    private GPSPoint home;

    public Passenger(String name, int ID, String email, String telephone, GPSPoint home) {
        this.name = name;
        this.ID = ID;
        this.email = email;
        this.telephone = telephone;
        this.home = home;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public GPSPoint getHome() {
        return home;
    }

    public void setHome(GPSPoint home) {
        this.home = home;
    }

    @Override
    public String toString() {
        return "Passenger{" + "name=" + name + ", ID=" + ID + ", email=" + email + ", telephone=" + telephone + ", home=" + home + '}';
    }
    
    
    
}
