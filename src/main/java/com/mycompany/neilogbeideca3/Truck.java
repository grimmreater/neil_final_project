/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.neilogbeideca3;

import java.util.Date;

/**
 *
 * @author Neil Ogbeide
 */
public class Truck extends Vehicle {
    private double loadSpaceSize;

    public Truck(double loadSpaceSize, String make, String model, double mpkw, int seatNo, String regNo, int cpm, Date Lastservicedate, int milage, GPSPoint VDPoint) {
        super(make, model, mpkw, seatNo, regNo, 10, Lastservicedate, milage, VDPoint);
        this.loadSpaceSize = loadSpaceSize;
    }

    public double getLoadSpaceSize() {
        return loadSpaceSize;
    }

    public void setLoadSpaceSize(double loadSpaceSize) {
        this.loadSpaceSize = loadSpaceSize;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Truck other = (Truck) obj;
        if (Double.doubleToLongBits(this.loadSpaceSize) != Double.doubleToLongBits(other.loadSpaceSize)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Truck{" + "loadSpaceSize=" + loadSpaceSize + '}';
    }
    
    

    
}
