/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.neilogbeideca3;

/**
 *
 * @author Neil Ogbeide
 */
import java.io.IOException;
import java.util.Scanner;
import java.util.ArrayList;

/*
Significant assisstance from Tomas Smith, Aaron Richardson and Cameron Scholes
*/


public class Main {
    public static void main(String[] args) throws IOException 
    {
        ArrayList<Passenger> passengerList = new ArrayList<>();
        ArrayList<Vehicle> vehicleList = new ArrayList<>();
        ArrayList<Booking> bookingList = new ArrayList<>();
        start(passengerList, vehicleList, bookingList);
        
        
    }

    public static void start(ArrayList<Passenger> passengerList, ArrayList<Vehicle> vehicleList, ArrayList<Booking> bookingList) throws IOException {
        
        
        PassengerEditor pe = new PassengerEditor();
        BookingManager  bm = new BookingManager();
        VehicleRet vr = new VehicleRet();
        
        System.out.println("1] Bookings");
        System.out.println("2] Paseengers");
        System.out.println("3] Vehicles");
        System.out.println("4] Quit");
        Scanner sc = new Scanner(System.in);
        int choice = sc.nextInt();
        
        pe.readPassenger();
        bm.readBooking();
        vr.readVehicle();
        
        
        switch (choice) {
            case 1:
                System.out.println("1] View Bookings");
                System.out.println("2] Add Bookings");
                System.out.println("3] Edit Bookings");
                System.out.println("4] Get Passenger Bookings by Date");
                System.out.println("5] Quit");
                choice = sc.nextInt();
                
                 switch (choice) {
            case 1:
                bm.getBookingList();
                break;
                
            case 2:
                    bm.AddBooking(passengerList, vehicleList);
                    bm.writeBooking();
                    break;
            case 3:
                bm.editBooking(passengerList, vehicleList);
                bm.writeBooking();
                break;
                 
            case 4:
                bm.getPassengerBookings(passengerList);
                 break;
                 
            case 5:
                
                break;
                 }
                break;
                
                
            case 2:
                System.out.println("1] View Passengers");
                System.out.println("2] Add Passengers");
                System.out.println("3] Edit Passengers");
                System.out.println("4] Quit");
                choice = sc.nextInt();
                
        switch (choice) {
            case 1:
                pe.getPassengerList();
                break;
            case 2:
                pe.AddPassenger();
                pe.writePassenger();
                break;
            case 3:
                pe.EditPassenger();
                pe.writePassenger();
                break;
            case 4:
                
                break;
        }
                
                
                break;
            case 3:
                System.out.println("1] View Vehicles");
                System.out.println("2] Quit");
                choice = sc.nextInt();
                
                switch (choice){
                    case 1:
                        vr.createVehicleList();
                        vr.getVehicleList();
                        vr.writeVehicles();
                        vr.readVehicle();
                        break;
                    case 2:
                        
                        break;
                }
                break;
          /*  case 4:
                exit();
                break;
*/
            default:
                break;
        }
    }
}