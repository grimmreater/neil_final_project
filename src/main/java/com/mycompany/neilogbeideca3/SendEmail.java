/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.neilogbeideca3;

/**
 *
 * @author Neil Ogbeide
 */
import java.util.Properties;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 *
 * @author Neil Ogbeide
 */
public class SendEmail {
    private static String USERNAME = "ogbeide.neil";
    private static String PASSWORD = "p@55word";
    private static String RECIPIENT = "";//insert in passenger email here
    
    public static void main(String[] args) {
        String from = USERNAME;
        String pass = PASSWORD;
        String[] to = {RECIPIENT};
        String subject = "Your vehicle booking";
        String body = "Thanks for choosing gd2 cars";
        
        SendFromGmail(from, pass, to, subject, body);
    }
    
    public static void SendFromGmail(String from, String pass, String[] to, String subject, String body) {
        Properties props = System.getProperties();
        String host = "smtp.gmail.com";
        props.put("mail.smtp.starttls.enable" , "true");
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.user", from );
        props.put("mail.smtp.password", pass);
        props.put("mail.smtp.port", "587");
        props.put("mail.smtp.auth", "true");
        
        Session session = Session.getDefaultInstance(props);
        MimeMessage message = new MimeMessage(session);
        
        try{
            message.setFrom(new InternetAddress(from));
            InternetAddress[] toAddress = new InternetAddress[to.length];
            
            //Deal with possible array of addresses
            for(int i=0; i< toAddress.length; i++){
                message.addRecipient(Message.RecipientType.TO, toAddress[i]);
            }
            message.setSubject(subject);
            message.setText(body);
            Transport transport = session.getTransport("smtp");
            transport.connect(host, from, pass);
            transport.sendMessage(message, message.getAllRecipients());
            transport.close();
        }
        
        catch(Address Exception ae){
            ae.printStackTrace();
        }
        catch(MessagingException me){
            me.printStackTrace();
        }
    }
    
}
