/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.neilogbeideca3;

/**
 *
 * @author Neil Ogbeide
 */
public class Longitude {
    private double longPoint;

    public Longitude(double longPoint) {
        this.longPoint = longPoint;
    }

    public double getLongPoint() {
        return longPoint;
    }

    @Override
    public String toString() {
        return "Longitude{" + "longPoint=" + longPoint + '}';
    }
    
    
    
}
