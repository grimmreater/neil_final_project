/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.neilogbeideca3;

/**
 *
 * @author Neil Ogbeide
 */
public class Latitude {
    private double latPoint;

    public Latitude(double latPoint) {
        this.latPoint = latPoint;
    }

    public double getLatPoint() {
        return latPoint;
    }

    @Override
    public String toString() {
        return "Latitude{" + "latPoint=" + latPoint + '}';
    }
    
    
}
