/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.neilogbeideca3;

/**
 *
 * @author Neil Ogbeide
 */


import java.util.Date;
import java.util.Objects;


public class Vehicle {
    private String make;
    private String model;
    private double mpkw;
    private int seatNo;
    private String regNo;
    private int cpm;
    private Date Lastservicedate;
    private int milage;

    public void setMake(String make) {
        this.make = make;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setMpkw(double mpkw) {
        this.mpkw = mpkw;
    }

    public void setSeatNo(int seatNo) {
        this.seatNo = seatNo;
    }

    public void setRegNo(String regNo) {
        this.regNo = regNo;
    }

    public void setCpm(int cpm) {
        this.cpm = cpm;
    }

    public void setLastservicedate(Date Lastservicedate) {
        this.Lastservicedate = Lastservicedate;
    }

    public void setMilage(int milage) {
        this.milage = milage;
    }

    public void setVDPoint(GPSPoint VDPoint) {
        this.VDPoint = VDPoint;
    }
    private GPSPoint VDPoint;

    public Vehicle(String make, String model, double mpkw, int seatNo, String regNo, int cpm, Date Lastservicedate, int milage, GPSPoint VDPoint) {
        this.make = make;
        this.model = model;
        this.mpkw = mpkw;
        this.seatNo = seatNo;
        this.regNo = regNo;
        this.cpm = cpm;
        this.Lastservicedate = Lastservicedate;
        this.milage = milage;
        this.VDPoint = VDPoint;
    }

    public String getMake() {
        return make;
    }

    public String getModel() {
        return model;
    }

    public double getMpkw() {
        return mpkw;
    }

    public int getSeatNo() {
        return seatNo;
    }

    public String getRegNo() {
        return regNo;
    }

    public int getCpm() {
        return cpm;
    }

    public Date getLastservicedate() {
        return Lastservicedate;
    }

    public int getMilage() {
        return milage;
    }

    public GPSPoint getVDPoint() {
        return VDPoint;
    }

    @Override
    public String toString() {
        return "Vehicle{" + "make=" + make + ", model=" + model + ", mpkw=" + mpkw + ", seatNo=" + seatNo + ", regNo=" + regNo + ", cpm=" + cpm + ", Lastservicedate=" + Lastservicedate + ", milage=" + milage + ", VDPoint=" + VDPoint + '}';
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 37 * hash + Objects.hashCode(this.make);
        hash = 37 * hash + Objects.hashCode(this.model);
        hash = 37 * hash + (int) (Double.doubleToLongBits(this.mpkw) ^ (Double.doubleToLongBits(this.mpkw) >>> 32));
        hash = 37 * hash + this.seatNo;
        hash = 37 * hash + Objects.hashCode(this.regNo);
        hash = 37 * hash + this.cpm;
        hash = 37 * hash + Objects.hashCode(this.Lastservicedate);
        hash = 37 * hash + this.milage;
        hash = 37 * hash + Objects.hashCode(this.VDPoint);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Vehicle other = (Vehicle) obj;
        if (Double.doubleToLongBits(this.mpkw) != Double.doubleToLongBits(other.mpkw)) {
            return false;
        }
        if (this.seatNo != other.seatNo) {
            return false;
        }
        if (this.cpm != other.cpm) {
            return false;
        }
        if (this.milage != other.milage) {
            return false;
        }
        if (!Objects.equals(this.make, other.make)) {
            return false;
        }
        if (!Objects.equals(this.model, other.model)) {
            return false;
        }
        if (!Objects.equals(this.regNo, other.regNo)) {
            return false;
        }
        if (!Objects.equals(this.Lastservicedate, other.Lastservicedate)) {
            return false;
        }
        if (!Objects.equals(this.VDPoint, other.VDPoint)) {
            return false;
        }
        return true;
    }
    
    
    
}
