/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.neilogbeideca3;

/**
 *
 * @author Neil Ogbeide
 */

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectOutputStream;
import static java.lang.Math.abs;
import static java.lang.Math.sqrt;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;



public class BookingManager {    
    
    public ArrayList<Booking> bookingList;
    Scanner sc = new Scanner(System.in);
    DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
    Date bookingDate;
     GPSPoint start;
     GPSPoint end;
     Passenger myPassenger;
     Vehicle myvehicle;
     FileWriter bookFile;
     


    
    public BookingManager() throws IOException
    {
        this.bookFile = new FileWriter("bookings.txt");
        this.bookingList = new ArrayList<>();
    }

    
    
    public void AddBooking(ArrayList<Passenger> passengerList, ArrayList<Vehicle> vehicleList){
        
        int ID = bookingList.size() + 1;
        
            System.out.println("Enter in the values of your new booking: ");
            System.out.println("Enter passenger ID: ");
            int passID= sc.nextInt();
            
                
            for (int k=0;k<passengerList.size();++k ){
              if(passID == passengerList.get(k).getID()){
              myPassenger = passengerList.get(k);
              }
              }
                
                
              try{
            System.out.println("Enter date in format dd/MM/yyyy: ");
            String date= sc.nextLine();
            Date bookingDate = sdf.parse(date); 
              }
              catch(ParseException e){
              e.printStackTrace();
              }
            
            System.out.println("Enter start longitude: ");
            Double StartLong= sc.nextDouble();
            System.out.println("Enter start latitude: ");
            Double StartLat= sc.nextDouble();
            
            System.out.println("Enter end longitude: ");
            Double EndLong= sc.nextDouble();
            System.out.println("Enter end latitude: ");
            Double EndLat= sc.nextDouble();
            GPSPoint start = new GPSPoint(StartLong,StartLat);
            GPSPoint end = new GPSPoint(EndLong,EndLat);
            
            
            
            System.out.println("Enter the type of Vehicle wanted: ");
            System.out.println("0-49] Car");
            System.out.println("50-59] 4x4");
            System.out.println("60-69] Van");
            System.out.println("70-79] Truck");
            
            
            int choice= sc.nextInt();
            
             if(choice >=0 && choice <50 ){
             myvehicle = vehicleList.get(choice);
              }
             else if(choice >=50 && choice <59 ){
             myvehicle = vehicleList.get(choice);
              }
             else if(choice >=60 && choice <69 ){
             myvehicle = vehicleList.get(choice);
              }
             else if(choice >=70 && choice <79 ){
             myvehicle = vehicleList.get(choice);
              }
             
             
    // adapted from https://www.movable-type.co.uk/scripts/latlong.html
    double Radius = 6371e3; // metres
    double φ1 = Math.toRadians(start.getLatPoint());
    double φ2 = Math.toRadians(end.getLatPoint());
    double Δφ = φ2-φ1;
    double λ1= Math.toRadians(start.getLongPoint());
    double λ2=Math.toRadians(end.getLongPoint());
    double Δλ = λ2-λ1;

    double x = (Δλ) * Math.cos((φ1+φ2)/2);
    double y = Δφ;
    double distance = abs(Math.sqrt(x*x + y*y) * Radius);
    double initialDistance = 0;
    double avg=0;
       
    
    for(int j=1; j<=bookingList.size();++j){
    
    initialDistance= initialDistance+distance;
    avg = initialDistance/j;
    }
        System.out.println("The avaerage distance for the bookings is " + avg + "km");
             
             
            
            
            double cost = distance * vehicleList.get(choice).getCpm();
            
            
            Booking myBooking = new Booking(ID, myPassenger, bookingDate, start, end, cost, myvehicle);
            bookingList.add(myBooking);
        }
    
    
    
    
        
    //for q8
   
    
    
    
 /*   
    public void distance(){
    double Radius = 6371e3; // metres
    double φ1 = Math.toRadians(start.getLatPoint());
    double φ2 = Math.toRadians(end.getLatPoint());
    double Δφ = φ2-φ1;
    double λ1= Math.toRadians(start.getLongPoint());
    double λ2=Math.toRadians(end.getLongPoint());
    double Δλ = λ2-λ1;

    double x = (Δλ) * Math.cos((φ1+φ2)/2);
    double y = Δφ;
    double distance = abs(Math.sqrt(x*x + y*y) * Radius);
    double initialDistance = 0;
    double avg=0;
       
    
    for(int i=1; i<=bookingList.size();++i){
    
    initialDistance= initialDistance+distance;
    avg = initialDistance/i;
    }
        System.out.println("The avaerage distance for the bookings is " + avg + "km");

}
   */     
        
    public ArrayList<Booking> getBookingList() {
        return bookingList;
    }
    
    

    
    
    public Passenger getPassengerBookings(ArrayList<Passenger> passengerList){
        System.out.println("Enter passenger ID: ");
            int passID= sc.nextInt();
            
                for (int k=0;k<passengerList.size();++k ){
              if(passID == passengerList.get(k).getID()){
                  System.out.println(bookingList.get(k));
                  
   Collections.sort(bookingList, new Comparator<Booking>() {
  public int compare(Booking o1, Booking o2) {
      return o1.getBookingTime().compareTo(o2.getBookingTime());
  }
});
                  
              }
              }
return passengerList.get(passID);
    }
   
    
    public void editBooking(ArrayList<Passenger> passengerList, ArrayList<Vehicle> vehicleList) {
        System.out.println("Select an ID that you wish to edit: ");
        int ID = sc.nextInt();
        
        
         if( !bookingList.contains(ID)){
        throw new IllegalArgumentException("Booking doesn't exist within the list");
        }
         
         System.out.println("Enter in the values to change to your booking: ");
            System.out.println("Enter passenger ID: ");
            int passID= sc.nextInt();

         
         for (int k=0;k<passengerList.size();++k ){
              if(passID == passengerList.get(k).getID()){
              myPassenger = passengerList.get(k);
              }
              }
            
              
                
                
              try{
            System.out.println("Enter date in format dd/MM/yyyy: ");
            String date= sc.nextLine();
            Date bookingDate = sdf.parse(date); 
              }
              catch(ParseException e){
              e.printStackTrace();
              }
            
            
            
            System.out.println("Enter start longitude: ");
            Double StartLong= sc.nextDouble();
            System.out.println("Enter start latitude: ");
            Double StartLat= sc.nextDouble();
            
            System.out.println("Enter end longitude: ");
            Double EndLong= sc.nextDouble();
            System.out.println("Enter end latitude: ");
            Double EndLat= sc.nextDouble();
            GPSPoint start = new GPSPoint(StartLong,StartLat);
            GPSPoint end = new GPSPoint(EndLong,EndLat);
            
            
            
            System.out.println("Enter the type of Vehicle wanted: ");
            System.out.println("0-49] Car");
            System.out.println("50-59] 4x4");
            System.out.println("60-69] Van");
            System.out.println("70-79] Truck");
            
            
            int choice= sc.nextInt();
            
             if(choice >=0 && choice <50 ){
             myvehicle = vehicleList.get(choice);
              }
             else if(choice >=50 && choice <59 ){
             myvehicle = vehicleList.get(choice);
              }
             else if(choice >=60 && choice <69 ){
             myvehicle = vehicleList.get(choice);
              }
             else if(choice >=70 && choice <79 ){
             myvehicle = vehicleList.get(choice);
              }
             
             
    double Radius = 6371e3; // metres
    double φ1 = Math.toRadians(start.getLatPoint());
    double φ2 = Math.toRadians(end.getLatPoint());
    double Δφ = φ2-φ1;
    double λ1= Math.toRadians(start.getLongPoint());
    double λ2=Math.toRadians(end.getLongPoint());
    double Δλ = λ2-λ1;

    double x = (Δλ) * Math.cos((φ1+φ2)/2);
    double y = Δφ;
    double distance = abs(Math.sqrt(x*x + y*y) * Radius);
    double initialDistance = 0;
    double avg=0;
       
    
    for(int j=1; j<=bookingList.size();++j){
    
    initialDistance= initialDistance+distance;
    avg = initialDistance/j;
    }
        System.out.println("The avaerage distance for the bookings is " + avg + "km");
             
             
            
            
            double cost = distance * vehicleList.get(choice).getCpm();
            
            
            
            

            
            
            Booking myBooking = new Booking(ID, myPassenger, bookingDate, start, end, cost, myvehicle);
         
         
         bookingList.set(ID-1, myBooking);
         
        
        
    }
    
    public void DeleteBooking(){
    {
        System.out.println("Select an ID you wish to delete: ");
        int myBooking = sc.nextInt();
        
        bookingList.remove(myBooking -1);
        
    }
        
    }

    
    public void writeBooking() throws IOException{
        
    FileWriter write = new FileWriter("bookings.txt");
                BufferedWriter outStream = new BufferedWriter(write);
                for (int k = 0; k < bookingList.size(); k++)
                outStream.write(bookingList.get(k).toString());   
                outStream.close();
                System.out.println("Data saved.");
    }
    
    // adapted from https://www.dotnetperls.com/split-java   
    public void readBooking() throws FileNotFoundException, IOException{
     BufferedReader reader = new BufferedReader(new FileReader ("bookings.txt"));

        while (true) {
            String line = reader.readLine();
            if (line == null) {
                break;
            }

            String[] parts = line.split("}}");
            for (String part : parts) {
                //System.out.println(part);
            }
            System.out.println();
        }

        reader.close();
     }

}