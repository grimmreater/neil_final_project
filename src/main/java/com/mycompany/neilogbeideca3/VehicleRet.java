    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.neilogbeideca3;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Neil Ogbeide
 */
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

public class VehicleRet {
    
    private ArrayList<Vehicle> vehicleList;
    
    
    
    public VehicleRet(){
        this.vehicleList = new ArrayList<>();
    }

    
    
    GPSPoint VDPoint = new GPSPoint(42.1379,23.897);

    
    
    public ArrayList<Vehicle> createVehicleList() {
     
        
        try {
            DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            String servDate ="13/07/18";
            Date lastservicedate= sdf.parse(servDate);
            
            
            String make = "Tesla";
            int carMax=50;
            for(int i=0; i<carMax; i++){
                String model= "Model 3";
                Random carRand = new Random();
                int carReg1= carRand.nextInt(8) + 10;
                int carReg2 = carRand.nextInt(88888)+ 11111;
                String fullReg = Integer.toString(carReg1) + "-D-" + Integer.toString(carReg2);
                
                int milage = carRand.nextInt(5000)+ 1000;
                
                Car myCar = new Car(make, model, 5, 6, fullReg, 2, lastservicedate, milage, VDPoint);
                vehicleList.add(myCar);
            }
            int FourbyFourMax=10;
            for(int i=0; i<FourbyFourMax; i++ ){
                String model ="Model S";
                Random FourRand = new Random();
                int FourReg1= FourRand.nextInt(8) + 10;
                int FourReg2 = FourRand.nextInt(88888)+ 11111;
                String fullReg = Integer.toString(FourReg1) + "-D-" + Integer.toString(FourReg2);
                
                int milage = FourRand.nextInt(5000)+ 1000;
                
                FourByFour myFour = new FourByFour(make, model, 4, 7, fullReg, 4, lastservicedate, milage, VDPoint);
                vehicleList.add(myFour);
            }
            
            int VanMax = 10;
            for(int i=0; i<VanMax; i++ ){
                String model ="Model V";
                Random VanRand = new Random();
                int VanReg1= VanRand.nextInt(8) + 10;
                int VanReg2 = VanRand.nextInt(88888)+ 11111;
                String fullReg = Integer.toString(VanReg1) + "-D-" + Integer.toString(VanReg2);
                
                int milage = VanRand.nextInt(5000)+ 1000;
                double loadSpace=20.0;
                
                Van myVan = new Van(loadSpace, make, model, 4, 7, fullReg, 6, lastservicedate, milage, VDPoint);
                vehicleList.add(myVan);
            }
            
            int TruckMax = 10;
            for(int i=0; i<TruckMax; i++ ){
                String model ="Da Big1";
                Random TruckRand = new Random();
                int TruckReg1= TruckRand.nextInt(8) + 10;
                int TruckReg2 = TruckRand.nextInt(88888)+ 11111;
                String fullReg = Integer.toString(TruckReg1) + "-D-" + Integer.toString(TruckReg2);
                
                int milage = TruckRand.nextInt(5000)+ 1000;
                double loadSpace=100.0;
                
                Truck myTruck = new Truck(loadSpace, make, model, 4, 7, fullReg, 10, lastservicedate, milage, VDPoint);
                vehicleList.add(myTruck);
            }    
            return vehicleList;
        } catch (ParseException ex) {
            Logger.getLogger(VehicleRet.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println(vehicleList);
        return vehicleList;
        
        
}
    public ArrayList<Vehicle> getVehicleList() {
        return vehicleList;
    }
    
    
     public void writeVehicles() throws IOException{
        
  //  FileWriter write =  new FileWriter("vehicles.txt");
    
   // final String write = "vehicles.txt";
		
		 			
		FileWriter write = new FileWriter("vehicles.txt");
                BufferedWriter outStream = new BufferedWriter(write);
                for (int k = 0; k < vehicleList.size(); k++)
                outStream.write(vehicleList.get(k).toString());   
                outStream.close();
                System.out.println("Data saved.");
			
			
		}


// adapted from https://www.dotnetperls.com/split-java     
     public void readVehicle() throws FileNotFoundException, IOException{
     BufferedReader reader = new BufferedReader(new FileReader ("vehicles.txt"));

        while (true) {
            String line = reader.readLine();
            if (line == null) {
                break;
            }
           
            String[] parts = line.split("}}");
            for (String part : parts) {
//                System.out.println(part);
            }
            System.out.println();
        }

        reader.close();
     }

    
}
    