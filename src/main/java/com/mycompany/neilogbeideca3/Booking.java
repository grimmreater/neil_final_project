/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.neilogbeideca3;

/**
 *
 * @author Neil Ogbeide
 */

import java.util.Date;

public class Booking {
    
    private int bookID;
    private Passenger passenger;
    private Date bookingTime;
    private GPSPoint start;
    private GPSPoint end;
    private double cost;
    private Vehicle vehicle;

    public Booking(int bookID, Passenger passenger, Date bookingTime, GPSPoint start, GPSPoint end, double cost, Vehicle vehicle) {
        this.bookID = bookID;
        this.passenger = passenger;
        this.bookingTime = bookingTime;
        this.start = start;
        this.end = end;
        this.cost = cost;
        this.vehicle = vehicle;
    }

    public int getBookID() {
        return bookID;
    }

    public Passenger getPassenger() {
        return passenger;
    }

    public Date getBookingTime() {
        return bookingTime;
    }

    public GPSPoint getStart() {
        return start;
    }

    public GPSPoint getEnd() {
        return end;
    }

    public double getCost() {
        return cost;
    }

    public Vehicle getVehicle() {
        return vehicle;
    }

    
    
    
    
    
    
}
