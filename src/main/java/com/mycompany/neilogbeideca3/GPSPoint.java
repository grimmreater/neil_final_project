/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.neilogbeideca3;

/**
 *
 * @author Neil Ogbeide
 */
public class GPSPoint {
    private double longPoint;
    private double latPoint;

    public GPSPoint(double longPoint, double latPoint) {
        this.longPoint= longPoint;
        this.latPoint = latPoint;
    }

    public double getLatPoint() {
        return latPoint;
    }

    public double getLongPoint() {
        return longPoint;
    }

    public void setLongPoint(double longPoint) {
        this.longPoint = longPoint;
    }

    public void setLatPoint(double latPoint) {
        this.latPoint = latPoint;
    }

    
    
    
    @Override
    public String toString() {
        return "GPSPoint{" +  ", longPoint=" + longPoint + " latPoint=" + latPoint + '}';
    }

}