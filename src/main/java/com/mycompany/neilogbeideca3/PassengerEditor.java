/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.neilogbeideca3;

/**
 *
 * @author Neil Ogbeide
 */

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

public class PassengerEditor {
    
    private ArrayList<Passenger> passengerList;
    Scanner sc = new Scanner(System.in);
    
    public PassengerEditor()
    {
        this.passengerList = new ArrayList<>();
    }
        
    
    public void AddPassenger(){
    
    
       
            int ID = passengerList.size() + 1;
        
    System.out.println("Enter in the values that you wish to add to a new passenger: ");
    System.out.println("Enter name: ");
    String name = sc.nextLine();
    
    System.out.println("Enter email: ");
    String email= sc.nextLine();
    
    System.out.println("Enter telephone: ");
    String telephone= sc.nextLine();
    
    System.out.println("Enter home longitude: ");
    double GPSPointLong= sc.nextDouble();
    System.out.println("Enter home latitude: ");
    double GPSPointLat= sc.nextDouble();
    
    GPSPoint myGPSPoint = new GPSPoint(GPSPointLong, GPSPointLat);
    
    
    
     /*   
        @Override
        public String toString() {
        return "PassengerEditor{" + "GPSPointString=" + GPSPointString + '}';
        }
    */
     
     Passenger myPassenger = new Passenger(name, ID, email, telephone, myGPSPoint);
     if( myPassenger==null ){ 
            throw new IllegalArgumentException("Passenger information is doesn't exist");
     }
        if( passengerList.contains(myPassenger)){
        throw new IllegalArgumentException("Passenger exists within the list");
        }
     
     passengerList.add(myPassenger);
        
}
    
    public List<Passenger> getPassengerList(){
        System.out.println(passengerList);
    return this.passengerList;
    }
    
    public void DeletePassenger(){
    {
        System.out.println("Select an ID you wish to delete: ");
        int myPassenger = sc.nextInt();
        
        passengerList.remove(myPassenger);
        
    }
    }
    
    public void EditPassenger(){
        System.out.println("Select an Id that you wish to edit: ");
        int ID = sc.nextInt();
        
   /*      if( ID==null ){ 
            throw new IllegalArgumentException("Passenger information is doesn't exist");
     }
*/
        if( !passengerList.contains(ID)){
        throw new IllegalArgumentException("Passenger doesn't exist within the list");
        }
        
    System.out.println("Enter in the values that you wish to change to this passenger: ");
    System.out.println("Enter name: ");
    String name = sc.nextLine();
    System.out.println("Enter email: ");
    String email= sc.nextLine();
    System.out.println("Enter telephone: ");
    String telephone= sc.nextLine();
    System.out.println("Enter home longitude: ");
    Double GPSPointLong= sc.nextDouble();
    System.out.println("Enter home latitude: ");
    Double GPSPointLat= sc.nextDouble();
    
    GPSPoint myGPSPoint = new GPSPoint(GPSPointLong, GPSPointLat);
        
    Passenger myPassenger = new Passenger(name, ID, email, telephone, myGPSPoint);
    passengerList.set(ID, myPassenger);
    
    }
     
 
    public void writePassenger() throws IOException{
        
   FileWriter write = new FileWriter("passengers.txt");
                BufferedWriter outStream = new BufferedWriter(write);
                for (int k = 0; k < passengerList.size(); k++)
                outStream.write(passengerList.get(k).toString());   
                outStream.close();
                System.out.println("Data saved.");
    
}
    // adapted from https://www.dotnetperls.com/split-java   
    public void readPassenger() throws FileNotFoundException, IOException{
     BufferedReader reader = new BufferedReader(new FileReader ("passengers.txt"));

        while (true) {
            String line = reader.readLine();
            if (line == null) {
                break;
            }
            
            String[] parts = line.split("}}");
            for (String part : parts) {
//                System.out.println(part);
            }
            System.out.println();
        }

        reader.close();
     }
    
}